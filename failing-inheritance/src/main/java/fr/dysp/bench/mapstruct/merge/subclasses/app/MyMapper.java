package fr.dysp.bench.mapstruct.merge.subclasses.app;

import fr.dysp.bench.mapstruct.merge.subclasses.lib.ChildSrc;
import fr.dysp.bench.mapstruct.merge.subclasses.lib.ChildTgt;
import fr.dysp.bench.mapstruct.merge.subclasses.lib.Extra;
import fr.dysp.bench.mapstruct.merge.subclasses.lib.ParentSrc;
import fr.dysp.bench.mapstruct.merge.subclasses.lib.ParentTgt;
import org.mapstruct.InheritConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.SubclassMapping;

@Mapper
public abstract class MyMapper {

    @SubclassMapping(target = ChildTgt.class, source = ChildSrc.class)
    @Mapping(target = "extra", source = "extra.foo")
    @Mapping(target = "ptf1", source = "src.psf1")
    public abstract ParentTgt mapParents(ParentSrc src, Extra extra);
    
    @InheritConfiguration
    @Mapping(target = "ctf1", source = "src.csf1")
    public abstract ChildTgt mapChildren(ChildSrc src, Extra extra);
    
}
