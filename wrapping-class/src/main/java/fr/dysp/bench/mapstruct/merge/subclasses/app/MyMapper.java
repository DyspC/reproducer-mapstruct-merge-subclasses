package fr.dysp.bench.mapstruct.merge.subclasses.app;

import fr.dysp.bench.mapstruct.merge.subclasses.lib.ChildSrc;
import fr.dysp.bench.mapstruct.merge.subclasses.lib.ChildTgt;
import fr.dysp.bench.mapstruct.merge.subclasses.lib.Extra;
import fr.dysp.bench.mapstruct.merge.subclasses.lib.ParentSrc;
import fr.dysp.bench.mapstruct.merge.subclasses.lib.ParentTgt;
import org.mapstruct.InheritConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.SubclassMapping;
import org.mapstruct.factory.Mappers;

@Mapper
public abstract class MyMapper {

    public static final MyMapper INSTANCE = Mappers.getMapper(MyMapper.class);

    public static class ParentPair {
        public ParentSrc node;
        public Extra extra;

        public ParentPair(ParentSrc node, Extra extra) {
            this.node = node;
            this.extra = extra;
        }
    }
    public static class ChildPair extends ParentPair {

        public ChildPair(ChildSrc node, Extra extra) {
            super(node, extra);
        }
        // Force type to pass mapstruct compile checks
        public ChildSrc getNode() {
            return ((ChildSrc) node);
        }
    }

    @SubclassMapping(target = ChildTgt.class, source = ChildPair.class)
    @Mapping(target = "extra", source = "pair.extra.foo")
    @Mapping(target = "ptf1", source = "pair.node.psf1")
    public abstract ParentTgt mapParents(ParentPair pair);
    
    @InheritConfiguration
    @Mapping(target = "ctf1", source = "pair.node.csf1")
    public abstract ChildTgt mapChildren(ChildPair pair);
    
}
