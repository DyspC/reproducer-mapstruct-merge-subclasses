# Test bench for a mapstruct issue

## Reproducers

- [Basic but have to check types before mapping](./simple-no-subclass/src/main/java/fr/dysp/bench/mapstruct/merge/subclasses/app/App.java)  
    `./gradlew clean :simple-no-subclass:run`
- [Main reproduction (does not compile)](./failing-inheritance/Readme.md)  
  `./gradlew clean :failing-inheritance:classes`
- [Workaround that handles inheritance but adds a useless object layer and thus type checks](./wrapping-class/src/main/java/fr/dysp/bench/mapstruct/merge/subclasses/app/App.java)  
  `./gradlew clean :wrapping-class:run`

## Problem

It is possible to bind several source objects on a mapper method

```java
    @Mapping(target = "extra", source = "extra.foo")
    @Mapping(target = "ptf1", source = "src.psf1")
    public abstract ParentTgt mapParents(ParentSrc src, Extra extra);
```

It is also possible to declare an alternative mapping strategy for subclasses

```java
    @SubclassMapping(target = ChildTgt.class, source = ChildSrc.class)
    @Mapping(target = "ptf1", source = "src.psf1")
    public abstract ParentTgt mapParents(ParentSrc src);
    
    @InheritConfiguration
    @Mapping(target = "ctf1", source = "src.csf1")
    public abstract ChildTgt mapChildren(ChildSrc src);
```

Though, combining the 2 options results in mapstruct creating a method that does not compile

```java
    @Override
    public ParentTgt mapParents(ParentSrc src, Extra extra) {
        if ( src == null && extra == null ) {
            return null;
        }

        if (src instanceof ChildSrc) {
            return childSrcToChildTgt( (ChildSrc) src );
        }
        else {
            String ptf1 = null;
            if ( src != null ) {
                ptf1 = src.psf1;
            }

            ParentTgt parentTgt = new ParentTgt( ptf1 );

            if ( extra != null ) {
                parentTgt.extra = extra.foo;
            }

            return parentTgt;
        }
    }

    @Override
    public ChildTgt mapChildren(ChildSrc src, Extra extra) {
        if ( src == null && extra == null ) {
            return null;
        }

        String ctf1 = null;
        String ptf1 = null;
        if ( src != null ) {
            ctf1 = src.csf1;
            ptf1 = src.psf1;
        }

        ChildTgt childTgt = new ChildTgt( ptf1, ctf1 );

        if ( extra != null ) {
            childTgt.extra = extra.foo;
        }

        return childTgt;
    }

    protected ChildTgt childSrcToChildTgt(ChildSrc childSrc) {
        if ( childSrc == null ) {
            return null;
        }

        String ptf1 = null;

        ptf1 = childSrc.psf1;

        String ctf1 = null;

        ChildTgt childTgt = new ChildTgt( ptf1, ctf1 );

        childTgt.extra = childSrc.foo; // childSrc.foo does not exist

        return childTgt;
    }
```